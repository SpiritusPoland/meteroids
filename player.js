function Player()
{
    this.x=300;
    this.y=300;
    this.w=45;
    this.h=45;
    this.vector=[];
    this.speed=0.58;
    this.speedX=0;
    this.speedY=0;
    this.angle=0;
    this.angleRadian=0;    
    this.img=new Image();
    this.img.src='assets/images/ship.png';
}

Player.prototype.moveForward=function()
{
    if(this.speedX<=5 && this.speedX>=-5)
    this.speedX+=Math.sin(this.angleRadian)*this.speed;
    if(this.speedY<=5 && this.speedY>=-5 )
    this.speedY+=-Math.cos(this.angleRadian)*this.speed;
    if(this.speedX>5)
        this.speedX=5;
    if(this.speedX<-5)
        this.speedX=-5;
    if(this.speedY>5)
        this.speedY=5;
    if(this.speedY<-5)
        this.speedY=-5;    
    
        
}



Player.prototype.move=function()
{
    if(keys[65]==true)
    this.rotateLeft();
    if(keys[68]==true)
    this.rotateRight();
    if(keys[87]==true)
    this.moveForward();
    this.x+=this.speedX;
    this.y+=this.speedY;
    if(this.x>600)
    {
        this.x=0;
    }
    if (this.x<0)
    {
        this.x=600;
    }
    if(this.y>600)
        this.y=0;
    if(this.y<0)
        this.y=600;
        this.speedX-=this.speedX*0.02;
        this.speedY-=this.speedY*0.02;

}

Player.prototype.moveBackward=function()
{

}

Player.prototype.rotateLeft=function()
{
this.angle-=5;
    if(this.angle<=0)
    {
        this.angle=360;
    }
this.toRadian();
//this.img.style.transform='rotate('+this.angle+'deg)';
}

Player.prototype.rotateRight=function()
{
    this.angle+=5;
    if(this.angle>=360)
    {    
        this.angle=0;
    }
    this.toRadian();
  //  this.img.style.transform='rotate('+this.angle+'deg)';

}



Player.prototype.toRadian=function()
{
    var pi=Math.PI;
    this.angleRadian=this.angle*(pi/180);
}

Player.prototype.shot=function()
{
    console.log("strzał");
shots.push(new Shot(this.x,this.y,this.angleRadian,shots.length));
console.log("l");
}

