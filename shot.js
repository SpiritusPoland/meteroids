function Shot(x,y,angleRadian,index)
{

    this.index=index;
    this.xDistance=0;
    this.yDistance=0;
    this.speed=7;
    this.xSpeed=Math.sin(angleRadian)*this.speed;
    this.ySpeed=-Math.cos(angleRadian)*this.speed;
    this.h=10;
    this.w=10;
    
    
    this.angleRadian=angleRadian;    
    this.x=x;
    this.y=y;
    
    this.img=new Image();
    this.img.src='assets/images/shot.png';
    this.visible=true;
}

Shot.prototype.move=function(i)
{
    this.xDistance+=this.xSpeed;
    this.yDistance+=this.ySpeed;
    this.x+=this.xSpeed;
    this.y+=this.ySpeed;
    if(this.x>600)
    this.x=0;
    if(this.y>600)
    this.y=0;
    if(this.y<0)
    this.y=600;
    if(this.x<0)
    this.x=600;
    if(Math.sqrt(this.xDistance*this.xDistance+this.yDistance*this.yDistance)>800)
        shots.splice(i,1);
        

}

