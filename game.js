var render = function()//funkcja renderujaca obraz
{
    context.drawImage(bgImage,0,0,600,600);
	drawRotated(heroo);
	shots.forEach(element => {
		context.drawImage(element.img,element.x-element.w/2,element.y-element.h/2,element.w,element.h);
	});

	
	
    }

var main = function()
{    
    render();
    requestAnimationFrame(main);
	heroo.move();
	console.log("sots: " +shots.length);
	for(i=0;i<shots.length;i++)
	{
		shots[i].move(i);
		
	}
	
}

var drawRotated=function drawRotatedImage(player) { 
	context.save();  
	context.translate(player.x, player.y);
 
	// rotate around that point, converting our 
	// angle from degrees to radians 
	context.rotate(player.angleRadian);
 	// draw it up and to the left by half the width
	// and height of the image 
	context.drawImage(player.img, -player.w/2, -player.h/2,player.w,player.h);
 	// and restore the co-ords to how they were when we began
	context.restore(); 
}

var drawTranslated=function drawTranslatedImage(shot)
{
	context.save()
	context.translate(shot.x,shot.y);
	context.drawImage(shot.img, -shot.w/2,-shot.h/2,shot.w,shot.h);
	context.restore();
}